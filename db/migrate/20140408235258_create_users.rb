class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :login, null: false
      t.string :nome, null: false
      t.string :email, null: false
      t.string :password, null: false
      t.boolean :admin, default: false

      t.timestamps
    end
  end
end
